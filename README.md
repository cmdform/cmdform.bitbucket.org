Schemo Cmd Form
===============

Cmd line like forms for schemo

Build
-----

The project requires the  [grunt.js](https://github.com/cowboy/grunt) build tool. If you have node installed just run `npm install -g grunt` to setup grunt, then `grunt` in the root of the project to build.

Usage
-----

Call these to begin

```javascript
new CmdForm();
```

See /examples for some static demos

Source Structure
----------------

See /lib for the source files

* _cmd-form.js_ Manipulates the dom and carries the state of the plugin