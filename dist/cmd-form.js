/*! Schemo CMD Form - v0.1.0 - 2012-11-21
* * Copyright (c) 2012 */

var CmdForm;

(function (exports) {
  if (typeof module !== 'undefined' && module.exports) {
    module.exports = exports; // CommonJS
  } else if (typeof define === 'function') {
    define(['jquery'], exports); // AMD
  } else {
    CmdForm = exports; // <script>
  }
}((function ($) {
  'use strict';


  var inputOptions = {'class': 'cmd-line', 'spellcheck': 'false'};

  var labelTmpl = function(num, label) {
    return $('<span/>', {
      'class': 'cmd-label ' + label + ' cmd' + num
    }).html('$&nbsp;' + label + ':&nbsp;');
  };

  var inputTmpl = function(num, property) {
    return $('<span />', {
      'class': 'cmd-input ' + property.name + ' cmd' + num,
      'data-name': property.name,
      'data-type': property.type ? property.type : "text",
      'contenteditable': true
    });
  };

  var blockSubmit = function (e){
    e.preventDefault();
    return true;
  };

  var Formulator = function core(el, options){
    var $el = $(el);

    $el.append(
      this.newLine(0, options.properties[0])
    );
    $('.cmd-input').last().focus();
    $el.on('submit', blockSubmit);

    $el.keypress(this.keypress(options.properties));
  };

  Formulator.prototype = {
    constructor: Formulator,
    newLine: function(num , property){
      switch (property.type){
        case 'password':
          return $('<div />', 
            inputOptions
          )
          .append(labelTmpl(num, property.name))
          .append(
            inputTmpl(num, property)
            .css(property.obs ? property.obs :
            {'color': 'rgb(17, 18, 17)'})
          );
        case 'text':
          return $('<div />', 
            inputOptions
          ).append(labelTmpl(num, property.name)).append(inputTmpl(num, property));
        default:
          return $('<div />', 
            inputOptions
          ).append(labelTmpl(num, property.name)).append(inputTmpl(num, property));
      }
    },
    /**
    * wokr out value for input
    */
    determine: function($el){
      switch ($el.attr('data-type')) {
        case "text":
          break;
        case "password":
          break;
      }
    },
    collect: function($el){
      var values = [];
      $el.find('.cmd-input').each(function(i, el){
        if($(el).attr('data-name')){
          values.push({
            name: $(el).attr('data-name'),
            value: $(el).text()
          });
        }
      });
      return values;
    },
    sendOption: function(results, $el, success, error){
      var options = {
        type: $el.attr("method") ? $el.attr("method") : 'POST',
        url: $el.attr("action"),
        success: success,
        error: error
      };
      var data = '';
      $.each(results, function(i, ob) {
        data += ob.name + '=' + ob.value;
        if(i + 1 !== results.length) {
          data += '&';
        }
      });
      options.data = data;
      return options;
    },
    keypress: function(properties){
      var that = this,
        property = 1;

      return function(e) {
        if ((e.keyCode ? e.keyCode : e.which) === 13) {
          e.preventDefault();
          var $el = $(e.target).closest('form');

          if( properties[property] === undefined){
            $.ajax(
              that.sendOption(
                that.collect($el), $el, function(data){console.log('success', data);}, function(error){console.log('error', error);}
            ));
          } else {
            $el.append(that.newLine(property, properties[property]));
            $('.cmd-input').last().focus();
            property++;
          }
        } else {
          return true;
        }
      };
    }
  };


  return Formulator;

}(jQuery))));