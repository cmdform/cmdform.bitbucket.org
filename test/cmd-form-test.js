/**
* Test the low level functions
*
*/

(function(window, document, $, CmdForm, QUnit, undefined){
  'use strict';


  test('initialisation', function(){

    var form = new CmdForm($('#testCmd'), {
      properties: [{name: 'username'}, {name: 'password'}, {name: 'remember me'}]
    });

    ok(typeof form==='object' , 'init ok');

  });


  test('new line on enter', function(){

    var options = {
      'properties': [{name: 'username'}, {name: 'password'}, {name: 'remember me'}]
    };

    var $el = $('#testCmd'), form = new CmdForm($el, options);

    $el.find('.cmd-input.username').trigger({ type : 'keypress', which : 13 });

    equal($el.find('.cmd-input').length, 2, 'enter should generate a second input line');

    $el.find('.cmd-input.password').trigger({ type : 'keypress', which : 13 });

    equal($el.find('.cmd-input').length, 3, 'enter should generate a third input line');

  });

  test('password type', function(){

    var options = {
      'properties': [{name: 'password', type: 'password'}]
    };

    var $el = $('#testCmd'), form = new CmdForm($el, options);

    var $password = $el.find('.cmd-input.password');

    equal($password.css('color'), $('body').css('backgroundColor'), 'color should be used to obsfuckate the password');
  });

  test('names of inputs', function(){

    var options = {
      'properties': [{name: 'username'}, {name: 'password'}, {name: 'rememberMe'}]
    };

    var $el = $('#testCmd'), form = new CmdForm($el, options);

    $el.find('.cmd-input').last().text('aaa');

    $el.find('.cmd-input').last().trigger({ type : 'keypress', which : 13 });

    $el.find('.cmd-input').last().text('bbb');

    $el.find('.cmd-input').last().trigger({ type : 'keypress', which : 13 });

    $el.find('.cmd-input').last().text('y');

    var result = form.collect($el);

    
    equal(result[0].name, 'username');
    equal(result[1].name, 'password');
    equal(result[2].name, 'rememberMe');

    equal(result[0].value, 'aaa');
    equal(result[1].value, 'bbb');
    equal(result[2].value, 'y');

  });

  test('ajax options', function(){

    var options = {
      'properties': [{name: 'username'}, {name: 'password'}, {name: 'rememberMe'}]
    };

    var $el = $('#testCmd'), form = new CmdForm($el, options);

    var data = [
      {name: 'username', value: 'aaa'},
      {name: 'password', value: 'bbb'},
      {name: 'rememberMe', value: 'y'}
    ];

    var result = form.sendOption(data, $el, function(){});

    console.log(result);

    equal(result.data, 'username=aaa&password=bbb&rememberMe=y', 'form style should match');
  });

}(window, document, jQuery, CmdForm, QUnit));